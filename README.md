# General Info #

To check if the string is translated we use:


```
#!xml

t="t" - Translated
t="f" - Not translated
```
